# README #

A C++ based desktop application.

This is an activity manager. It's main objective is to help the user manage his daily activities.

The main functionalities of the app are:
    
    - add/delete/update/search activity
    
    - filter/sort activities by title/description/type
    
    - export list of selected activities in computer
    
    - undo the last operation
    
Some ss's from the app:

List of activities and CRUD operations:

![Alt text](https://bitbucket.org/chstr_mihai/dailyactivities/raw/5be25356a84989eff7a58ca348e99edd44699b3f/appSS/activitiesCRUD.JPG)

Filter operations on the list:

![Alt text](https://bitbucket.org/chstr_mihai/dailyactivities/raw/5be25356a84989eff7a58ca348e99edd44699b3f/appSS/activitiesFilters.JPG)

Search activity by name:

![Alt text](https://bitbucket.org/chstr_mihai/dailyactivities/raw/5be25356a84989eff7a58ca348e99edd44699b3f/appSS/activitiesSearch.JPG)

Create and export activity list:

![Alt text](https://bitbucket.org/chstr_mihai/dailyactivities/raw/5be25356a84989eff7a58ca348e99edd44699b3f/appSS/activitiesExport.JPG)