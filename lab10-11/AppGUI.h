#pragma once
#include <string>
#include <vector>

#include "domain.h"
#include "service.h"

#include <QtWidgets/qwidget.h>
#include <QtWidgets/qboxlayout.h>
#include <QtWidgets/qpushbutton.h>
#include <QtWidgets/qlabel.h>
#include <QtWidgets/qlineedit.h>
#include <QtWidgets/qformlayout.h>
#include <QtWidgets/qlistwidget.h>
#include <QtWidgets/qtabwidget.h>
#include <QtWidgets/qmessagebox.h>
#include <QtWidgets/qgridlayout.h>
#include <QtWidgets/qstackedlayout.h>
#include <qdebug.h>
#include <qtablewidget.h>
#include <QStyleFactory>
#include <qstyleplugin.h>
#include <qheaderview.h>

class activityGUI : public QWidget {

public:

	activityGUI(service& s) : srv{ s } {

		initGUI();

		load_all_in_list();

		initConnect();
	
	}

private:
	service& srv;

	QListWidget* activityList = new QListWidget;
	QPushButton* exitButton = new QPushButton("Exit");
	QPushButton* addButton = new QPushButton("Add");
	QPushButton* deleteButton = new QPushButton("Delete");
	QPushButton* updateButton = new QPushButton("Update");
	QPushButton* undoButton = new QPushButton("<-");
	QLineEdit* titleTextBox = new QLineEdit;
	QLineEdit* decriptionTextBox = new QLineEdit;
	QLineEdit* typeTextBox = new QLineEdit;
	QLineEdit* lengthTextBox = new QLineEdit;
	QPushButton* titleSortButton = new QPushButton("Title");
	QPushButton* descriptionSortButton = new QPushButton("Description");
	QPushButton* typeSortButton = new QPushButton("Type");
	QPushButton* searchButton = new QPushButton("Search");
	QLineEdit* searchTextBox = new QLineEdit;
	QPushButton* searchedDeleteButton = new QPushButton("Delete");
	QPushButton* searchedUpdateButton = new QPushButton("Update");
	QPushButton* searchedCloseButton = new QPushButton("Close");
	QWidget* searchWindow = new QWidget;
	QLabel* searchedTitle = new QLabel;
	QLineEdit* searchedDescriptionTextBox = new QLineEdit;
	QLineEdit* searchedTypeTextBox = new QLineEdit;
	QLineEdit* searchedLengthTextBox = new QLineEdit;
	QPushButton* descriptionFilterButton = new QPushButton("Description");
	QPushButton* typeFilterButton = new QPushButton("Type");
	QLineEdit* filterTextBox = new QLineEdit;
	QPushButton* saveFilterButton = new QPushButton("Save");
	QPushButton* discardFilterButton = new QPushButton("Discard");
	bool is_filtered = false;

	QTableWidget* currentActivityTable = new QTableWidget{ 0, 4 };
	QTableWidget* todaysActivityTable = new QTableWidget{ 0, 4 };
	QString tableSelectedActivityTitle = "";

	QPushButton* addToCurrentButton = new QPushButton("Add to today's list");
	QPushButton* emptyCurrentButton = new QPushButton("Empty today's list");
	QPushButton* randomToCurrentButton = new QPushButton("Add random");
	QLineEdit* exportFileTextBox = new QLineEdit;
	QPushButton* exportFileButton = new QPushButton("Export");
	QWidget* randomWidget = new QWidget;
	QLineEdit* randomTextBox = new QLineEdit;

	/*QVBoxLayout* activityButtonLayout = new QVBoxLayout;
	vector<QPushButton*> activityButtons;
	QPushButton* p;*/





	void load_all_in_list() {

		vector<activity> activities = srv.get_all();
		int l = 0;
		for (auto a : activities) {

			activityList->addItem(QString::fromStdString(a.get_title()));

			currentActivityTable->insertRow(l);
			currentActivityTable->setItem(l, 0, new QTableWidgetItem(QString::fromStdString(a.get_title())));
			currentActivityTable->setItem(l, 1, new QTableWidgetItem(QString::fromStdString(a.get_description())));
			currentActivityTable->setItem(l, 2, new QTableWidgetItem(QString::fromStdString(a.get_type())));
			currentActivityTable->setItem(l, 3, new QTableWidgetItem(QString::number(a.get_length())));
			currentActivityTable->setRowHeight(l, 15);
			l++;

		}

		//makeActivityProducts();

	}

	void initConnect() {
		
		QObject::connect(exitButton, &QPushButton::clicked, [&]() {

			close();

			});

		QObject::connect(addButton, &QPushButton::clicked, [&]() {

			auto title = titleTextBox->text(), description = decriptionTextBox->text(), type = typeTextBox->text(), length = lengthTextBox->text();
			try {

				srv.add(title.toStdString(), description.toStdString(), type.toStdString(), length.toInt());
				activityList->addItem(title);

				int row = currentActivityTable->rowCount();
				currentActivityTable->insertRow(row);
				currentActivityTable->setItem(row, 0, new QTableWidgetItem(title));
				currentActivityTable->setItem(row, 1, new QTableWidgetItem(description));
				currentActivityTable->setItem(row, 2, new QTableWidgetItem(type));
				currentActivityTable->setItem(row, 3, new QTableWidgetItem(length));
				currentActivityTable->setRowHeight(row, 15);
				
				is_filtered = false;

				//makeActivityProducts();

			}
			catch (const RepoException& ex) {

				QMessageBox::warning(nullptr, "Error", QString::fromStdString(ex.getMsg()));

			}
			catch (const ValidException& ex) {

				vector<string> err = ex.getMsg();
				QString error = "";
				for (auto s : err)
					error += QString::fromStdString(s) + " ";
				QMessageBox::warning(nullptr, "Error", error);

			}

			});

		QObject::connect(activityList, &QListWidget::itemSelectionChanged, [&]() {

			if (activityList->selectedItems().isEmpty()) {

				titleTextBox->setText("");
				decriptionTextBox->setText("");
				typeTextBox->setText("");
				lengthTextBox->setText("");
				return;

			}

			QListWidgetItem* selected = activityList->selectedItems().at(0);
			titleTextBox->setText(selected->text());
			auto act = srv.find(selected->text().toStdString(), "a");
			decriptionTextBox->setText(QString::fromStdString(act.get_description()));
			typeTextBox->setText(QString::fromStdString(act.get_type()));
			lengthTextBox->setText(QString::number(act.get_length()));

			});

		QObject::connect(currentActivityTable, &QTableWidget::itemSelectionChanged, [&]() {

			if (currentActivityTable->selectedItems().isEmpty()) {
			
				tableSelectedActivityTitle = "";
				return;
			
			}

			tableSelectedActivityTitle = currentActivityTable->selectedItems().at(0)->text();

			});

		QObject::connect(addToCurrentButton, &QPushButton::clicked, [&]() {

			if (tableSelectedActivityTitle == "")
				return;

			try {

				srv.add_to_current_list(tableSelectedActivityTitle.toStdString());

				vector<activity> current = srv.get_current();

				todaysActivityTable->setRowCount(0);
				int row = 0;
				for (auto a : current) {

					todaysActivityTable->insertRow(row);
					todaysActivityTable->setItem(row, 0, new QTableWidgetItem(QString::fromStdString(a.get_title())));
					todaysActivityTable->setItem(row, 1, new QTableWidgetItem(QString::fromStdString(a.get_description())));
					todaysActivityTable->setItem(row, 2, new QTableWidgetItem(QString::fromStdString(a.get_type())));
					todaysActivityTable->setItem(row, 3, new QTableWidgetItem(QString::number(a.get_length())));
					todaysActivityTable->setRowHeight(row, 15);
					row++;

				}

			}

			catch (const RepoException& ex) {
				QMessageBox::warning(nullptr, "Error", QString::fromStdString(ex.getMsg()));
			}

			});

		QObject::connect(randomToCurrentButton, &QPushButton::clicked, [&]() {

			//randomWidget->setFixedHeight(100);

			auto randomLayout = new QVBoxLayout;
			auto randomLabel = new QLabel("Type the number of activities to be added");
			auto randomConfirmButton = new QPushButton("Confirm");
			auto randomCancelButton = new QPushButton("Cancel");

			auto randomNumberLayout = new QHBoxLayout;

			randomLayout->addWidget(randomLabel);
			randomLayout->addWidget(randomTextBox);
			//randomNumberLayout->addWidget(randomTextBox);
			//randomNumberLayout->addWidget(randomConfirmButton);

			//randomLayout->addLayout(randomNumberLayout);

			randomLayout->addSpacing(15);
			auto randomCancelLayout = new QHBoxLayout;
			//randomCancelLayout->addSpacing(100);
			randomCancelLayout->addWidget(randomConfirmButton);
			randomCancelLayout->addWidget(randomCancelButton);
			
			randomLayout->addLayout(randomCancelLayout);

			randomWidget->setLayout(randomLayout);
			randomWidget->setFixedHeight(150);
			randomWidget->setFixedWidth(270);
			randomWidget->show();


			QObject::connect(randomCancelButton, &QPushButton::clicked, [&]() {

				randomTextBox->clear();
				randomWidget->close();

				});

			QObject::connect(randomConfirmButton, &QPushButton::clicked, [&]() {

				srv.random_activities(randomTextBox->text().toInt());

				vector<activity> current = srv.get_current();

				todaysActivityTable->setRowCount(0);
				int row = 0;
				for (auto a : current) {

					todaysActivityTable->insertRow(row);
					todaysActivityTable->setItem(row, 0, new QTableWidgetItem(QString::fromStdString(a.get_title())));
					todaysActivityTable->setItem(row, 1, new QTableWidgetItem(QString::fromStdString(a.get_description())));
					todaysActivityTable->setItem(row, 2, new QTableWidgetItem(QString::fromStdString(a.get_type())));
					todaysActivityTable->setItem(row, 3, new QTableWidgetItem(QString::number(a.get_length())));
					todaysActivityTable->setRowHeight(row, 15);
					row++;

				}

				randomTextBox->clear();
				randomWidget->close();

				});

			});

		QObject::connect(emptyCurrentButton, &QPushButton::clicked, [&]() {

			srv.empty_current_list();
			todaysActivityTable->setRowCount(0);

			});

		QObject::connect(exportFileButton, &QPushButton::clicked, [&]() {
			
			if (exportFileTextBox->text().isEmpty()) {

				QMessageBox::warning(nullptr, "Error", "The text box is empty! Type the file name and try again!");
				return;

			}

			QString text = exportFileTextBox->text();
			srv.export_to_file(text.toStdString());
			QMessageBox::information(nullptr, "Succes", "Today's activity list has been exported to" + text + " file");

			});

		QObject::connect(deleteButton, &QPushButton::clicked, [&]() {

			try {

				srv.del(titleTextBox->text().toStdString(), "a");
				activityList->clear();
				currentActivityTable->setRowCount(0);
				loadData();

				is_filtered = false;

				//makeActivityProducts();

			}

			catch (const RepoException& ex) {

				QMessageBox::warning(nullptr, "Error", QString::fromStdString(ex.getMsg()));

			}

			});

		QObject::connect(updateButton, &QPushButton::clicked, [&]() {

			try {

				srv.modify(titleTextBox->text().toStdString(), decriptionTextBox->text().toStdString(), typeTextBox->text().toStdString(), lengthTextBox->text().toInt());
				activityList->clear();
				currentActivityTable->setRowCount(0);
				loadData();

				is_filtered = false;

			}

			catch (const RepoException& ex) {

				QMessageBox::warning(nullptr, "Error", QString::fromStdString(ex.getMsg()));

			}

			catch (const ValidException& ex) {

				vector<string> err = ex.getMsg();
				QString error = "";
				for (auto s : err)
					error += QString::fromStdString(s) + " ";
				QMessageBox::warning(nullptr, "Error", error);

			}

			});

		QObject::connect(undoButton, &QPushButton::clicked, [&]() {
			try {

				srv.undo();
				activityList->clear();
				currentActivityTable->setRowCount(0);
				loadData();

				//makeActivityProducts();
			
			}

			catch (const ActivityException& ex) {

				QMessageBox::warning(nullptr, "Error", QString::fromStdString(ex.getMsg()));

			}

			});

		QObject::connect(titleSortButton, &QPushButton::clicked, [&]() {
			
			auto sortedList = srv.sort_by_title();
			activityList->clear();
			for (auto a : sortedList) {

				activityList->addItem(QString::fromStdString(a.get_title()));

			}

			srv.filter_save(sortedList, 0);
			is_filtered = false;

			//makeActivityProducts();

			});

		QObject::connect(descriptionSortButton, &QPushButton::clicked, [&]() {

			auto sortedList = srv.sort_by_description();
			activityList->clear();
			for (auto a : sortedList) {

				activityList->addItem(QString::fromStdString(a.get_title()));

			}

			srv.filter_save(sortedList, 0);
			is_filtered = false;

			//makeActivityProducts();

			});

		QObject::connect(typeSortButton, &QPushButton::clicked, [&]() {

			auto sortedList = srv.sort_by_type_and_length();
			activityList->clear();
			for (auto a : sortedList) {

				activityList->addItem(QString::fromStdString(a.get_title()));

			}

			srv.filter_save(sortedList, 0);
			is_filtered = false;

			//makeActivityProducts();

			});

		QObject::connect(searchedDeleteButton, &QPushButton::clicked, [&]() {
			
			try {

				srv.del(searchedTitle->text().toStdString(), "a");
				activityList->clear();
				currentActivityTable->setRowCount(0);
				loadData();
				searchWindow->close();

				is_filtered = false;

				//makeActivityProducts();
			
			}

			catch (const RepoException& ex) {

				QMessageBox::warning(nullptr, "Error", QString::fromStdString(ex.getMsg()));

			}

			});

		QObject::connect(searchedUpdateButton, &QPushButton::clicked, [&]() {

			try {

				srv.modify(searchedTitle->text().toStdString(), searchedDescriptionTextBox->text().toStdString(), searchedTypeTextBox->text().toStdString(), searchedLengthTextBox->text().toInt());
				activityList->clear();
				currentActivityTable->setRowCount(0);
				loadData();
				searchWindow->close();

				is_filtered = false;

			}

			catch (const RepoException& ex) {

				QMessageBox::warning(nullptr, "Error", QString::fromStdString(ex.getMsg()));

			}

			catch (const ValidException& ex) {

				vector<string> err = ex.getMsg();
				QString error = "";
				for (auto s : err)
					error += QString::fromStdString(s) + " ";
				QMessageBox::warning(nullptr, "Error", error);

			}

			});

		QObject::connect(searchedCloseButton, &QPushButton::clicked, [&]() {

			searchWindow->close();

			});

		QObject::connect(searchButton, &QPushButton::clicked, [&]() {

			try {
				/*
				QPalette p(palette());
				p.setColor(QPalette::Background, Qt::darkGray);
				searchWindow->setAutoFillBackground(true);
				searchWindow->setPalette(p);
				*/
				auto searchedItem = srv.find(searchTextBox->text().toStdString(), "a");
				//auto searchLayoutWindow = new QVBoxLayout;

				auto searchedItemLayout = new QFormLayout;
				searchedTitle->setText(QString::fromStdString(searchedItem.get_title()));
				searchedItemLayout->addRow(new QLabel("Title"), searchedTitle);

				searchedDescriptionTextBox->setText(QString::fromStdString(searchedItem.get_description()));
				searchedTypeTextBox->setText(QString::fromStdString(searchedItem.get_type()));
				searchedLengthTextBox->setText(QString::number(searchedItem.get_length()));

				searchedItemLayout->addRow(new QLabel("Description"), searchedDescriptionTextBox);
				searchedItemLayout->addRow(new QLabel("Type"), searchedTypeTextBox);
				searchedItemLayout->addRow(new QLabel("Length"), searchedLengthTextBox);

				auto searchedButtonsLayout = new QHBoxLayout;
				searchedButtonsLayout->addWidget(searchedDeleteButton);
				searchedButtonsLayout->addWidget(searchedUpdateButton);
				searchedButtonsLayout->addWidget(searchedCloseButton);
				searchedItemLayout->addRow(searchedButtonsLayout);
				

				searchWindow->setLayout(searchedItemLayout);
				searchWindow->show();

			}
			catch (const RepoException& ex) {

				QMessageBox::warning(nullptr, "Error", QString::fromStdString(ex.getMsg()));

			}

			});

		QObject::connect(descriptionFilterButton, &QPushButton::clicked, [&]() {

			if (filterTextBox->text() == "") {

				QMessageBox::warning(nullptr, "Error", "The text box is empty");
				return;

			}

			auto filtered = srv.filter_description(filterTextBox->text().toStdString());
			activityList->clear();

			for (auto a : filtered)
				activityList->addItem(QString::fromStdString(a.get_title()));

			is_filtered = true;

			});

		QObject::connect(typeFilterButton, &QPushButton::clicked, [&]() {

			if (filterTextBox->text() == "") {

				QMessageBox::warning(nullptr, "Error", "The text box is empty");
				return;

			}

			auto filtered = srv.filter_type(filterTextBox->text().toStdString());
			activityList->clear();

			for (auto a : filtered)
				activityList->addItem(QString::fromStdString(a.get_title()));

			is_filtered = true;

			});

		QObject::connect(saveFilterButton, &QPushButton::clicked, [&]() {

			vector<activity> filtered;

			for (int i = 0; i < activityList->count(); i++) {

				auto title = activityList->item(i)->text();
				auto act = srv.find(title.toStdString(), "a");
				filtered.push_back(act);

			}

			if (is_filtered) {

				srv.filter_save(filtered, 1);
				activityList->clear();
				currentActivityTable->setRowCount(0);
				loadData();

				is_filtered = false;

				//makeActivityProducts();

			}

			});

		QObject::connect(discardFilterButton, &QPushButton::clicked, [&]() {

			vector<activity> activities = srv.get_all();

			activityList->clear();
			for (auto a : activities)
				activityList->addItem(QString::fromStdString(a.get_title()));

			is_filtered = false;

			});

	}

	void loadData() {

		std::vector<activity> list = srv.get_all();
		int l = 0;
		for (auto a : list) {

			activityList->addItem(QString::fromStdString(a.get_title()));
			currentActivityTable->insertRow(l);
			currentActivityTable->setItem(l, 0, new QTableWidgetItem(QString::fromStdString(a.get_title())));
			currentActivityTable->setItem(l, 1, new QTableWidgetItem(QString::fromStdString(a.get_description())));
			currentActivityTable->setItem(l, 2, new QTableWidgetItem(QString::fromStdString(a.get_type())));
			currentActivityTable->setItem(l, 3, new QTableWidgetItem(QString::number(a.get_length())));
			currentActivityTable->setRowHeight(l, 15);
			l++;
		
		}

	}
/*
	void makeActivityProducts() {

		for (auto b : activityButtons)
			delete b;

		activityButtons.clear();

		for (int i = 0; i < activityList->count(); i++) {

			QString name = activityList->item(i)->text();

			auto btn = new QPushButton(name);

			QObject::connect(btn, &QPushButton::clicked, [this, btn, name]() {

				try {

					srv.del(name.toStdString(), "a");
					activityList->clear();
					currentActivityTable->setRowCount(0);
					loadData();

					is_filtered = false;

					//activityButtons.clear();

					makeActivityProducts();

				}

				catch (const RepoException& ex) {

					QMessageBox::warning(nullptr, "Error", QString::fromStdString(ex.getMsg()));

				}

				});

			activityButtons.push_back(btn);
			activityButtonLayout->addWidget(btn);

		}

	}
	*/

	void initGUI() {
		
		/*QPalette p(palette());
		p.setColor(QPalette::Background, Qt::darkCyan);
		setAutoFillBackground(true);
		setPalette(p);*/

		auto allLayout = new QHBoxLayout;

		auto mainLayout = new QVBoxLayout;
		setLayout(allLayout);

		allLayout->addLayout(mainLayout);
		//allLayout->addLayout(activityButtonLayout);
		
		auto mainTabWidget = new QTabWidget;
		auto mainTab1 = new QWidget;
		mainTabWidget->addTab(mainTab1, "All activities");
		mainLayout->addWidget(mainTabWidget);

		auto mainLayoutTab1 = new QVBoxLayout;
		mainTab1->setLayout(mainLayoutTab1);

		auto searchLayout = new QFormLayout;
		searchTextBox->setMinimumWidth(250);
		searchLayout->addRow(searchTextBox, searchButton);
		mainLayoutTab1->addLayout(searchLayout);

		auto listButtonsLayout = new QHBoxLayout;
		auto sortLabel = new QLabel("Sort by");
		sortLabel->setFixedWidth(42);
		listButtonsLayout->addWidget(sortLabel);
		listButtonsLayout->addSpacing(5);
		listButtonsLayout->addWidget(titleSortButton);
		listButtonsLayout->addWidget(descriptionSortButton);
		listButtonsLayout->addWidget(typeSortButton);
		listButtonsLayout->setSpacing(0);
		//mainLayoutTab1->addLayout(listButtonsLayout);

		activityList->setMinimumHeight(225);
		//mainLayoutTab1->addWidget(activityList);

		auto listLayout = new QVBoxLayout;
		listLayout->addLayout(listButtonsLayout);
		listLayout->addWidget(activityList);
		listLayout->setSpacing(0);
		mainLayoutTab1->addLayout(listLayout);


		auto tab1 = new QWidget;
		/*tab1->setAutoFillBackground(true);
		tab1->setPalette(p);*/
		auto tabLayout1 = new QHBoxLayout;
		
		auto activityWriteLayout = new QFormLayout;
		activityWriteLayout->addRow("Title", titleTextBox);
		activityWriteLayout->addRow("Description", decriptionTextBox);
		activityWriteLayout->addRow("Type", typeTextBox);
		activityWriteLayout->addRow("Length", lengthTextBox);

		auto activityButtonsLayout = new QFormLayout;
		activityButtonsLayout->addRow(addButton);
		activityButtonsLayout->addRow(deleteButton);
		activityButtonsLayout->addRow(updateButton);

		tabLayout1->addLayout(activityWriteLayout);
		tabLayout1->addLayout(activityButtonsLayout);
		tab1->setLayout(tabLayout1);

		auto activityActionsTab = new QTabWidget;
		activityActionsTab->addTab(tab1, "Activities");
		

		auto tab2 = new QWidget;
		auto tabLayout2 = new QVBoxLayout;

		auto filterLabel = new QLabel("Type the activity atribute then choose the filtering mode");
		auto atributesLayout = new QHBoxLayout;

		auto gridLayout = new QGridLayout;
		auto filterButtonsLayout = new QHBoxLayout;
		filterButtonsLayout->addWidget(descriptionFilterButton);
		filterButtonsLayout->addWidget(typeFilterButton);
		auto saveButtonsLayout = new QHBoxLayout;
		saveButtonsLayout->addWidget(saveFilterButton);
		saveButtonsLayout->addWidget(discardFilterButton);
		gridLayout->addLayout(filterButtonsLayout, 1, 0);
		auto filterSpacingLayout = new QVBoxLayout;
		filterSpacingLayout->addSpacing(10);
		gridLayout->addLayout(filterSpacingLayout, 2, 0);
		gridLayout->addLayout(saveButtonsLayout, 3, 0);

		tabLayout2->addWidget(filterLabel);
		atributesLayout->addWidget(new QLabel("Atribute"));
		atributesLayout->addWidget(filterTextBox);
		tabLayout2->addLayout(atributesLayout);
		tabLayout2->addLayout(gridLayout);

		tab2->setLayout(tabLayout2);

		activityActionsTab->addTab(tab2, "Filters");
		mainLayoutTab1->addWidget(activityActionsTab);

		auto exitLayout = new QHBoxLayout;

		/*auto gridLayout = new QGridLayout;
		gridLayout->addWidget(undoButton);
		gridLayout->addWidget(exitButton);

		mainLayout->addLayout(gridLayout);*/

		exitLayout->addSpacing(250);
		exitLayout->addWidget(undoButton);
		exitLayout->addWidget(exitButton);

		mainLayoutTab1->addLayout(exitLayout);

		searchWindow->setFixedWidth(400);
		searchWindow->setFixedHeight(170);

		auto mainTab2 = new QWidget;
		auto mainLayoutTab2 = new QVBoxLayout;
		mainTab2->setLayout(mainLayoutTab2);
		mainTabWidget->addTab(mainTab2, "Today's activities");
		mainLayoutTab2->addWidget(currentActivityTable);

		currentActivityTable->setSelectionBehavior(QAbstractItemView::SelectRows);

		currentActivityTable->setColumnWidth(0, 100);
		currentActivityTable->setColumnWidth(1, 100);
		currentActivityTable->setColumnWidth(2, 100);
		currentActivityTable->setColumnWidth(3, 98);
		currentActivityTable->setHorizontalHeaderItem(0, new QTableWidgetItem("Title"));
		currentActivityTable->setHorizontalHeaderItem(1, new QTableWidgetItem("Description"));
		currentActivityTable->setHorizontalHeaderItem(2, new QTableWidgetItem("Type"));
		currentActivityTable->setHorizontalHeaderItem(3, new QTableWidgetItem("Length"));
		currentActivityTable->verticalHeader()->setVisible(false);

		todaysActivityTable->setSelectionBehavior(QAbstractItemView::SelectRows);

		todaysActivityTable->setColumnWidth(0, 100);
		todaysActivityTable->setColumnWidth(1, 100);
		todaysActivityTable->setColumnWidth(2, 100);
		todaysActivityTable->setColumnWidth(3, 98);
		todaysActivityTable->setHorizontalHeaderItem(0, new QTableWidgetItem("Title"));
		todaysActivityTable->setHorizontalHeaderItem(1, new QTableWidgetItem("Description"));
		todaysActivityTable->setHorizontalHeaderItem(2, new QTableWidgetItem("Type"));
		todaysActivityTable->setHorizontalHeaderItem(3, new QTableWidgetItem("Length"));
		todaysActivityTable->verticalHeader()->setVisible(false);


		auto add_removeCurrentLayout = new QHBoxLayout;
		add_removeCurrentLayout->addSpacing(10);
		add_removeCurrentLayout->addWidget(addToCurrentButton);
		add_removeCurrentLayout->addSpacing(20);
		add_removeCurrentLayout->addWidget(randomToCurrentButton);
		add_removeCurrentLayout->addSpacing(20);
		add_removeCurrentLayout->addWidget(emptyCurrentButton);
		add_removeCurrentLayout->addSpacing(10);
		mainLayoutTab2->addLayout(add_removeCurrentLayout);

		mainLayoutTab2->addWidget(todaysActivityTable);

		auto exportLayout = new QHBoxLayout;
		auto exportInfoLabel = new QLabel("File name");
		exportLayout->addWidget(exportInfoLabel);
		exportLayout->addWidget(exportFileTextBox);
		exportLayout->addWidget(exportFileButton);

		mainLayoutTab2->addLayout(exportLayout);


		qApp->setStyle(QStyleFactory::create("Fusion"));

		QPalette darkPalette;
		darkPalette.setColor(QPalette::Window, QColor(53, 53, 53));
		darkPalette.setColor(QPalette::WindowText, Qt::white);
		darkPalette.setColor(QPalette::Base, QColor(25, 25, 25));
		darkPalette.setColor(QPalette::AlternateBase, QColor(53, 53, 53));
		darkPalette.setColor(QPalette::ToolTipBase, Qt::white);
		darkPalette.setColor(QPalette::ToolTipText, Qt::white);
		darkPalette.setColor(QPalette::Text, Qt::white);
		darkPalette.setColor(QPalette::Button, QColor(53, 53, 53));
		darkPalette.setColor(QPalette::ButtonText, Qt::white);
		darkPalette.setColor(QPalette::BrightText, Qt::red);
		darkPalette.setColor(QPalette::Link, QColor(42, 130, 218));

		darkPalette.setColor(QPalette::Highlight, QColor(42, 130, 218));
		darkPalette.setColor(QPalette::HighlightedText, Qt::black);

		qApp->setPalette(darkPalette);

		qApp->setStyleSheet("QToolTip { color: #ffffff; background-color: #2a82da; border: 1px solid white; }");
		

	}

};