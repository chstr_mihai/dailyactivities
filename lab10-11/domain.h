#pragma once
#include <string>
#include <utility>

using std::move;
using std::string;

class ActivityException {
	std::string msg;
public:
	ActivityException(const string& m) :msg{ m } {};
	string getMsg()const { return msg; }
};

class activity {
private:
	string title_, description_, type_;
	int length_;
public:
	activity(string title, string description, string type, const int length) noexcept : title_(move(title)), description_(move(description)), type_(move(type)), length_{ length }{}

	string get_title() const {
		/*
		Functia care returneaza titlul unei activitati
		Output: titlu - un string
		*/
		return title_;
	}

	string get_description() const {
		/*
		Functia care returneaza descrierea unei activitati
		Output: descriere - un string
		*/
		return description_;
	}

	string get_type() const {
		/*
		Functia care returneaza tipul unei activitati
		Output: tip - un string
		*/
		return type_;
	}

	int get_length() const noexcept {
		/*
		Functia care returneaza durata unei activitati
		Output: durata - un intreg
		*/
		return length_;
	}

	void set_length(const int val) noexcept {
		/*
		Functia care modifica durata unei activitati cu o valoare noua
		Input: val - un intreg
		*/
		length_ = val;
	}

	void set_description(const string val) noexcept {
		/*
		Functia care modifica durata unei activitati cu o valoare noua
		Input: val - un intreg
		*/
		description_ = val;
	}

	void set_type(const string val) noexcept {
		/*
		Functia care modifica durata unei activitati cu o valoare noua
		Input: val - un intreg
		*/
		type_ = val;
	}
};