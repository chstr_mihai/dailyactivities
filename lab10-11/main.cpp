#include "lab10-11.h"
#include <QtWidgets/QApplication>
#include "AppGUI.h"

#include "repo.h"
#include "service.h"
#include "valid.h"
#include "test.h"

int main(int argc, char *argv[])
{

	{

		repo rep;
		repoFile file_rep{ "activities.txt" };
		valid val;
		service srv{ file_rep, val };
		test test;

		test.test_all();

		QApplication a(argc, argv);
		activityGUI gui{ srv };
		gui.setMinimumWidth(400);
		gui.show();

		return a.exec();
	
	}

}
