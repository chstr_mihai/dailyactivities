#include "repo.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <qdebug.h>

using std::ostream;
using std::stringstream;
using std::vector;

int repo::len() const noexcept {
	/*
	Functia care returneaza lungimea vectorului
	*/
	return all_.size();
}

void repo::add(const activity& a) {
	/*
	Functia care adauga o activitate in lista
	Input: &a - adresa activitatii
	*/
	if (exist(a) >= 0) {
		throw RepoException("\nThe activity already exists in the list!\n");
	}
	all_.push_back(a);
}

int repo::exist(const activity& a) const {
	/*
	Functia care verifica daca exista o activitate in lista
	Input: &a - adresa activitatii
	Output: indice - indicele elementului daca exista, -1 - daca elementul nu exista
	*/
	int indice = 0;
	for (const auto& aux : all_) {
		if (aux.get_title() == a.get_title()) {
			return indice;
		}
		indice++;
	}
	return -1;
}

vector<activity>& repo::get_all() {
	/*
	Functia care intoarce adresa listei de activitati
	Output: &all - adresa listei
	*/
	return all_;
}

void repo::del(const activity& a) {
	/*
	Functia care sterge o activitate din lista daca exista
	Input: &a - o activitate
	Se arunca exceptie daca nu exista activitatea in lista
	*/
	const int indice = exist(a);
	if (indice == -1) {
		throw RepoException("\nThis activity doesn't exist in the list!\n");
	}
	all_.erase(all_.begin() + indice);
}

void repo::modify(const activity& a) {
	/*
	Functia care modifica o activitate din lista
	Input: &a - o activitate
	Se arunca exceptie daca nu exista activitatea in lista
	*/
	const int indice = exist(a);
	if (indice == -1) {
		throw RepoException("\nThis activity doesn't exist in the list!\n");
	}
	all_.at(indice).set_description(a.get_description());
	all_.at(indice).set_type(a.get_type());
	all_.at(indice).set_length(a.get_length());
}

activity& repo::find(const activity& a) {
	/*
	Functia care cauta o activitate in lista
	Input: &a - o activitate
	Output: all[indice] - activitatea
	Se arunca exceptie daca nu exista activitatea in lista
	*/

	auto it = std::find_if(all_.begin(), all_.end(), [a](const activity& act) {
		return act.get_title() == a.get_title();
		});
	if (it == all_.end()) {
		throw RepoException("\nThis activity doesn't exist in the list!\n");
	}
	return *it;
}

vector<activity> repo::filter_description(const string& desc) {
	/*
	Functia care filtreaza lista in functie de o descriere data
	Input: &desc - un string
	Output: filtr - lista filtrata cu activitati
	*/

	vector<activity> filter;
	for (auto a : all_) {
		if (a.get_description() == desc)
			filter.push_back(a);
	}
	return filter;
}

vector<activity> repo::filter_tip(const string& tip) {
	/*
	Functia care filtreaza lista in functie de o tip dat
	Input: &tip - un string
	Output: filtr - lista filtrata cu activitati
	*/
	vector<activity> filter;
	for (auto a : all_) {
		if (a.get_type() == tip)
			filter.push_back(a);
	}
	return filter;
}

ostream& operator<<(ostream& out, const RepoException& ex) {
	out << ex.msg;
	return out;
}


void repo::empty_current_list() noexcept {
	current_activities_.clear();
}

void repo::add_to_current_list(string title) {

	activity a{ title, "a", "a", 1 };
	const int indice = exist(a);
	if (indice == -1) {
		throw RepoException("\nThis activity doesn't exist in the list!\n");
	}

	a = all_.at(indice);
	for (auto act : current_activities_)
		if (act.get_title() == a.get_title())
			throw RepoException("The activity already exists in today's activity list!");

	current_activities_.push_back(a);

	/*int nr = 0;
	for (auto a : all_)
		if (a.get_title() == title) {
			current_activities_.push_back(a);
			nr++;
		}
	if (nr == 0)
		throw RepoException("\nNu exista activitati cu acest titlu\n");*/
}

void repo::add_act_to_current_list(const activity& act) {
	auto nr = 0;
	for (auto& a : all_)
		if (a.get_title() == act.get_title() && a.get_type() == act.get_type()) {
			current_activities_.push_back(a);
			nr++;
			return;
		}
}

bool repo::exist_elem_in_current(const activity& act) {
	for (auto a : current_activities_)
		if (a.get_title() == act.get_title() && a.get_type() == act.get_type())
			return true;
	return false;
}

vector<activity> repo::get_current() const {
	return current_activities_;
}




void repoFile::write_to_file() {

	std::ofstream o(file_name_);
	vector<activity> activities = repo::get_all();

	for (auto a : activities) {

		o << a.get_title() << " " << a.get_description() << " " << a.get_type() << " " << a.get_length() << "\n";

	}

	o.close();

}

void repoFile::load_from_file() {

	std::ifstream i(file_name_);
	const vector<activity> activities;
	string title, desc, type, length;
	repo::empty_list();

	while (i >> title) {

		i >> desc >> type >> length;
		activity a{ title, desc, type, stoi(length) };
		repo::add(a);

	}

	i.close();

}
