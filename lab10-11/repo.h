#pragma once
#include "domain.h"
#include <vector>
#include <fstream>

using std::vector;
using std::ostream;

class repoAbs {
private:

	virtual int exist(const activity& a) const = 0;

public:
	virtual int len() const noexcept = 0;

	virtual void add(const activity& a) = 0;

	virtual void del(const activity& a) = 0;

	virtual void modify(const activity& a) = 0;

	virtual activity& find(const activity& a) = 0;

	virtual vector<activity>& get_all() = 0;

	virtual vector<activity> filter_description(const string& desc) = 0;

	virtual vector<activity> filter_tip(const string& tip) = 0;

	virtual void empty_current_list() noexcept = 0;

	virtual void add_to_current_list(string title) = 0;

	virtual vector<activity> get_current() const = 0;

	virtual void add_act_to_current_list(const activity& act) = 0;

	virtual bool exist_elem_in_current(const activity& a) = 0;

	virtual void empty_file() = 0;

};


class repo : public repoAbs {
private:
	vector<activity> all_;
	vector<activity> current_activities_;

	/*
	Functia care verifica daca exista o activitate in lista
	Input: &a - adresa activitatii
	Output: indice - indicele elementului daca exista, -1 - daca elementul nu exista
	*/
	int exist(const activity& a) const override;
public:
	repo() noexcept = default;

	repo(const repo& ot) = delete;

	/*
	Functia care returneaza lungimea vectorului
	*/
	int len() const noexcept override;

	/*
	Functia care adauga o activitate in lista
	Input: &a - adresa activitatii
	*/
	void add(const activity& a) override;

	/*
	Functia care sterge o activitate din lista daca exista
	Input: &a - o activitate
	Se arunca exceptie daca nu exista activitatea in lista
	*/
	void del(const activity& a) override;

	/*
	Functia care modifica o activitate din lista
	Input: &a - o activitate
	Se arunca exceptie daca nu exista activitatea in lista
	*/
	void modify(const activity& a) override;

	/*
	Functia care cauta o activitate in lista
	Input: &a - o activitate
	Output: all[indice] - activitatea
	Se arunca exceptie daca nu exista activitatea in lista
	*/
	activity& find(const activity& a) override;

	/*
	Functia care intoarce adresa listei de activitati
	Output: &all - adresa listei
	*/
	vector<activity>& get_all() override;

	/*
	Functia care filtreaza lista in functie de o descriere data
	Input: &desc - un string
	Output: filtr - lista filtrata cu activitati
	*/
	vector<activity> filter_description(const string& desc) override;

	/*
	Functia care filtreaza lista in functie de o tip dat
	Input: &tip - un string
	Output: filtr - lista filtrata cu activitati
	*/
	vector<activity> filter_tip(const string& tip) override;

	void empty_list() {
		all_.clear();
	}

	/*
	Functia care goleste lista de activitati curente
	*/
	void empty_current_list() noexcept override;

	/*
	Functia care adauga elementele care au un string primit ca titlu in lista de activitati curente
	Input: titlu - string
	Arunca exceptie daca nu exista niciun element cu acest titlu
	*/
	void add_to_current_list(string title) override;

	/*
	Functia care returneaza lista de activitati curente
	Output: activitati_curente - lista de activitati curente
	*/
	vector<activity> get_current() const override;

	/*
	Functia care adauga in lista de activitati curente un element primit
	Input: a - activitatea
	*/
	void add_act_to_current_list(const activity& act) override;

	/*
	Functia care verifica daca exista un element in lista de activitati curente
	Input: a - activitatea
	Output: true - daca exista deja in lista, false - altfel
	*/
	bool exist_elem_in_current(const activity& a) override;

	void empty_file() {}

};



class repoFile : public repo {

private:

	string file_name_;

public:

	repoFile(const string& file) : file_name_{ file } {

		load_from_file();

	}

	~repoFile() = default;

	void load_from_file();

	void write_to_file();

	void empty_file() override {

		std::ofstream o(file_name_);
		o.close();
	
	}

	void add(const activity& a) override {

		load_from_file();
		repo::add(a);
		write_to_file();

	}

	void del(const activity& a) override {
		load_from_file();
		repo::del(a);
		write_to_file();
	}

	void modify(const activity& a) override {
		load_from_file();
		repo::modify(a);
		write_to_file();
	}

	vector<activity>& get_all() override {
		load_from_file();
		return repo::get_all();
	}

};


class RepoException {
	string msg;
public:
	RepoException(string m) :msg{ m } {}

	string getMsg()const { return msg; }

	friend ostream& operator<<(ostream& out, const RepoException& ex);
};