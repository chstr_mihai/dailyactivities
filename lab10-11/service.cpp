#include "service.h"
#include <iostream>
#include <random>
#include <algorithm>
#include <memory>
#include <vector>
#include <numeric>

using std::vector;

void service::add(const string& title, const string& description, const string& type, const int length) {
	/*
	Functia care creeaza o activitate, o valideaza si o trimite la repo pentru adaugare
	Input: &titlu - un string, &descriere - un string, &tip - un string, durata - un intreg
	*/
	activity a{ title, description, type, length };
	val_.validate(a);
	rep_.add(a);
	undoActions.push_back(std::make_unique<UndoAdauga>(rep_, a));
}

void service::undo() {
	if (undoActions.empty()) {
		throw ActivityException{ "\nOperatia undo nu se mai poate efectua\n" };
	}
	/*
	ActiuneUndo* act = undoActions.back();
	act->doUndo();
	undoActions.pop_back();
	delete act;
	*/
	// Varianta cu unique_ptr
	//obs: la obiecte unique_ptr nu putem face copie
	undoActions.back()->doUndo();
	undoActions.pop_back();
}

const vector<activity>& service::get_all() const noexcept {
	/*
	Functia care primeste lista de activitati din repo si o returneaza
	Output: rep.get_all() - vectorul cu activitati
	*/
	return rep_.get_all();
}

void service::del(const string& title, const string& type) {
	/*
	Functia care creeaza o activitate si o trimite spre repo pentru a fi stearsa din lista
	Input: titlu - un string, &tip - un string
	*/
	activity a{ title, "a", type, 1 };
	activity a_aux = rep_.find(a);
	rep_.del(a);
	undoActions.push_back(std::make_unique<UndoSterge>(rep_, a_aux));
}

void service::modify(const string& title, const string& new_description, const string& new_type, const int new_length) {
	/*
	Functia care creeaza o activitate si o trimite spre repo pentru a-i fi modificata durata
	Input: titlu - un string, &tip - un string, durata_noua - un intreg
	*/
	activity a{ title, new_description, new_type, new_length };
	activity a_aux = rep_.find(a);
	val_.validate(a);
	rep_.modify(a);
	undoActions.push_back(std::make_unique<UndoModifica>(rep_, a_aux));
}

activity& service::find(const string& title, const string& type) const {
	/*
	Functia care creeaza o activitate si o trimite spre repo pentru a fi cautata in vector
	Input: titlu - un string, &tip - un string
	Output: rep.find() - adresa activitatii
	*/
	activity a{ title, "a", type, 1 };
	return rep_.find(a);
}

vector<activity> service::filter_description(const string& desc) const {
	/*
	Functia care primeste lista sortata dupa o descriere data si o returneaza
	Input: &desc - un string
	Output: rep.filter_descriere(desc) - vectorul filtrat
	*/
	//return rep.filter_descriere(desc);
	vector<activity> all = rep_.get_all(), filter;
	std::copy_if(all.begin(), all.end(), std::back_inserter(filter), [desc](const activity& a) {
		return a.get_description() == desc;
		});
	return filter;
}

vector<activity> service::filter_type(const string& tip) const {
	/*
	Functia care primeste lista sortata dupa un tip dat si o returneaza
	Input: &tip - un string
	Output: rep.filter_tip(tip) - vectorul filtrat
	*/
	vector<activity> all = rep_.get_all(), filter;
	std::copy_if(all.begin(), all.end(), std::back_inserter(filter), [tip](const activity& a) {
		return a.get_type() == tip;
		});
	return filter;
}


void service::filter_save(vector<activity> activitati, int mode) {

	vector<activity> all = rep_.get_all(), for_undo;
	if (mode) {

		for (auto a1 : all) {

			int exista = 0;

			for (auto a2 : activitati) {

				if (a1.get_title() == a2.get_title()) {

					exista = 1;
					break;

				}

			}

			if (exista == 0)
				for_undo.push_back(a1);

		}

	}

	rep_.empty_list();
	rep_.empty_file();

	for (auto a : activitati)
		rep_.add(a);

	if (mode)
		undoActions.push_back(std::make_unique<UndoFilter>(rep_, for_undo));

}


vector<activity> service::sort_by_title() const {
	/*
	Functia care Sorteaza vectorul de ativitati dupa titlu
	Output: sort - vectorul sortat
	*/

	vector<activity> sort = rep_.get_all();
	std::sort(sort.begin(), sort.end(), [](const activity& a1, const activity& a2) {
		return a1.get_title() < a2.get_title();
		});

	/*
	vector<Activitate> sort = rep.get_all();
	for (int i = 0; i < (int)sort.size(); i++)
		for (int j = i + 1; j < (int)sort.size(); j++)
			if (sort[i].get_titlu() > sort[j].get_titlu()) {
				Activitate aux = sort[i];
				sort[i] = sort[j];
				sort[j] = aux;
			}
	*/

	return sort;
}

vector<activity> service::sort_by_description() const {
	/*
	Functia care Sorteaza vectorul de ativitati dupa descriere
	Output: sort - vectorul sortat
	*/

	vector<activity> sort = rep_.get_all();
	std::sort(sort.begin(), sort.end(), [](const activity& a1, const activity& a2) {
		return a1.get_description() < a2.get_description();
		});

	/*
	vector<Activitate> sort = rep.get_all();
	for (int i = 0; i < (int)sort.size(); i++)
		for (int j = i + 1; j < (int)sort.size(); j++)
			if (sort[i].get_descriere() > sort[j].get_descriere()) {
				Activitate aux = sort[i];
				sort[i] = sort[j];
				sort[j] = aux;
			}
	*/

	return sort;
}

vector<activity> service::sort_by_type_and_length() const {
	/*
	Functia care Sorteaza vectorul de ativitati dupa tip si durata
	Output: sort - vectorul sortat
	*/

	vector<activity> sort = rep_.get_all();
	std::sort(sort.begin(), sort.end(), [](const activity& a1, const activity& a2) {
		if (a1.get_type() != a2.get_type())
			return a1.get_type() < a2.get_type();
		else
			return a1.get_length() < a2.get_length();
		});

	/*
	vector<Activitate> sort = rep.get_all();
	for (int i = 0; i < (int)sort.size(); i++)
		for (int j = i + 1; j < (int)sort.size(); j++)
			if ((sort[i].get_tip() > sort[j].get_tip()) || (sort[i].get_tip() == sort[j].get_tip() && sort[i].get_durata() > sort[j].get_durata())) {
				Activitate aux = sort[i];
				sort[i] = sort[j];
				sort[j] = aux;
			}
	*/

	return sort;
}

void service::empty_current_list() const noexcept {
	rep_.empty_current_list();
}

void service::add_to_current_list(const string& title) {
	rep_.add_to_current_list(title);
}

const vector<activity> service::get_current() const {
	return rep_.get_current();
}

void service::random_activities(int nr_activities) const {
	auto all = rep_.get_all();
	while (nr_activities > 0 && all.empty() == false) {
		std::mt19937 mt{ std::random_device{}() };
		const std::uniform_int_distribution<> dist(0, all.size() - 1);
		const auto rnd_nr = dist(mt);
		if (rep_.exist_elem_in_current(all.at(rnd_nr))) {
			all.erase(all.begin() + rnd_nr);
		}
		else {
			rep_.add_act_to_current_list(all.at(rnd_nr));
			nr_activities -= 1;
		}
	}
}

int service::suma_durate() {
	auto l = rep_.get_current();
	vector<int> durate;
	for (auto a : l)
		durate.push_back(a.get_length());

	auto suma = std::accumulate(durate.begin(), durate.end(), 0);
	return suma;
}

void service::export_to_file(const string& file_name) {

	std::ofstream o(file_name);
	vector<activity> current = rep_.get_current();

	for (auto a : current)
		o << a.get_title() << "," << a.get_description() << "," << a.get_type() << "," << a.get_length() << "\n";

}