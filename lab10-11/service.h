#pragma once
#include "domain.h"
#include "repo.h"
#include "valid.h"
#include "undo.h"

#include <string>
#include <vector>
#include <memory>

class service {
private:
	repo& rep_;
	valid& val_;
	std::vector<std::unique_ptr<ActiuneUndo>> undoActions;

public:
	service(repo& rep, valid& val) noexcept :rep_{ rep }, val_{ val } {
	}
	service(const service& ot) = delete;

	/*
	Functia care primeste lista de activitati din repo si o returneaza
	Output: rep.get_all() - vectorul cu activitati
	*/
	const vector<activity>& get_all() const noexcept;

	/*
	Functia care creeaza o activitate, o valideaza si o trimite la repo pentru adaugare
	Input: &titlu - un string, &descriere - un string, &tip - un string, durata - un intreg
	*/
	void add(const string& title, const string& description, const string& type, const int length);

	/*
	Functia care creeaza o activitate si o trimite spre repo pentru a fi stearsa din lista
	Input: titlu - un string, &tip - un string
	*/
	void del(const string& title, const string& type);

	/*
	Functia care creeaza o activitate si o trimite spre repo pentru a-i fi modificata durata
	Input: titlu - un string, &tip - un string, durata_noua - un intreg
	*/
	void modify(const string& title, const string& new_description, const string& type, const int new_length);

	/*
	Functia care creeaza o activitate si o trimite spre repo pentru a fi cautata in vector
	Input: titlu - un string, &tip - un string
	Output: rep.find() - adresa activitatii
	*/
	activity& find(const string& title, const string& type) const;

	/*
	Functia care primeste lista sortata dupa o descriere data si o returneaza
	Input: &desc - un string
	Output: rep.filter_descriere(desc) - vectorul filtrat
	*/
	vector<activity> filter_description(const string& desc) const;

	/*
	Functia care primeste lista sortata dupa un tip dat si o returneaza
	Input: &tip - un string
	Output: rep.filter_tip(tip) - vectorul filtrat
	*/
	vector<activity> filter_type(const string& tip) const;

	void filter_save(vector<activity> activitati, int mode);

	/*
	Functia care Sorteaza vectorul de ativitati dupa titlu
	Output: rep.filter_tip(tip) - vectorul sortat
	*/
	vector<activity> sort_by_title() const;

	/*
	Functia care Sorteaza vectorul de ativitati dupa descriere
	Output: sort - vectorul sortat
	*/
	vector<activity> sort_by_description() const;

	/*
	Functia care Sorteaza vectorul de ativitati dupa tip si durata
	Output: sort - vectorul sortat
	*/
	vector<activity> sort_by_type_and_length() const;

	/*
	Functia care goleste lista de activitati curente
	*/
	void empty_current_list() const noexcept;

	/*
	Functia care trimite la repo un string pentru a fi adaugate elementele cu acela ca titlu in lista de activitati curente
	Input: titlu - string
	*/
	void add_to_current_list(const string& title);

	/*
	Functia care returneaza lista de activitati curente
	Output: rep.get_current() - lista de activitati curente
	*/
	const vector<activity> get_current() const;

	/*
	Functia care adauga in lista de activitati curente un numar intreg primit de activitati random din lista actuala,
	daca numarul e mai mare decat numarul de activitati care pot si adaugate, atunci se adauga toate aceste
	*/
	void random_activities(int nr_activities) const;

	void undo();

	int suma_durate();

	void export_to_file(const string& file_name);

};