#include "test.h"
#include <assert.h>
#include <sstream>

using std::stringstream;
using std::ostream;

void test::test_create() {
	activity a{ "a", "b", "c", 1 };
	assert(a.get_title() == "a");
	assert(a.get_description() == "b");
	assert(a.get_type() == "c");
	assert(a.get_length() == 1);
	a.set_length(2);
	assert(a.get_length() == 2);
}

void test::test_valid() {
	valid v;
	activity inv{ "", "", "", -1 };
	try {
		v.validate(inv);
	}
	catch (const ValidException& ex) {
		std::stringstream sout;
		sout << ex;
		const auto mesaj = sout.str();
		assert(mesaj.find("negativ") > 0);
		assert(mesaj.find("vid") > 0);
	}
	activity a{ "a", "b", "c", 1 };
	v.validate(a);
}

void test::test_repo_add() {
	repo rep;
	activity a{ "a", "b", "c", 1 };
	assert(rep.len() == 0);
	rep.add(a);
	assert(rep.len() == 1);
	try {
		rep.add(a);
		assert(false);
	}
	catch (const RepoException&) {
		assert(true);
	}
	const activity b{ "b", "b", "b", 2 };
	rep.add(b);
	assert(rep.len() == 2);

	const auto v = rep.get_all();
	assert(int(v.size()) == rep.len());

}

void test::test_repo_delete() {
	repo rep;
	const activity a{ "a", "b", "c", 1 }, b{ "b", "b", "b", 2 };
	rep.add(a);
	rep.add(b);
	assert(rep.len() == 2);
	rep.del(b);
	assert(rep.len() == 1);
	rep.del(a);
	assert(rep.len() == 0);
	try {
		rep.del(a);
		assert(false);
	}
	catch (const RepoException&) {
		assert(true);
	}
}

void test::test_repo_modify() {
	repo rep;
	activity a{ "a", "b", "c", 1 }, new_a{ "a", "b", "c", 11 }, b{ "z", "z", "z", 12 };
	rep.add(a);
	auto v = rep.get_all();
	assert(v.at(0).get_length() == 1);
	rep.modify(new_a);
	v = rep.get_all();
	assert(v.at(0).get_length() == 11);
	try {
		rep.modify(b);
		assert(false);
	}
	catch (const RepoException&) {
		assert(true);
	}
}

void test::test_repo_find() {
	repo rep;
	activity a{ "a", "b", "c", 1 }, b{ "z", "z", "z", 12 };
	rep.add(a);
	assert(rep.find(a).get_title() == a.get_title());
	assert(rep.find(a).get_type() == a.get_type());
	assert(rep.find(a).get_description() == a.get_description());
	assert(rep.find(a).get_length() == a.get_length());
	try {
		rep.find(b);
		assert(false);
	}
	catch (const RepoException&) {
		assert(true);
	}

	try {
		rep.find(b);
		assert(false);
	}
	catch (const RepoException& e) {
		stringstream os;
		os << e;
		assert(os.str().find("exista") >= 0);
	}
}

void test::test_repo_filter_description() {
	repo rep, aux;
	activity a{ "a", "b", "c", 1 }, b{ "f", "g", "h", 2 }, c{ "e", "b", "z", 9 };
	rep.add(a);
	rep.add(b);
	rep.add(c);
	aux.add(a);
	aux.add(c);
	auto filter = rep.filter_description("b"), sum = aux.get_all();
	for (auto i = 0; i < 2; i++) {
		assert(filter.at(i).get_title() == sum.at(i).get_title());
		assert(filter.at(i).get_type() == sum.at(i).get_type());
		assert(filter.at(i).get_description() == sum.at(i).get_description());
		assert(filter.at(i).get_length() == sum.at(i).get_length());
	}
}

void test::test_repo_filter_tip() {
	repo rep, aux;
	activity a{ "a", "b", "c", 1 }, b{ "f", "g", "h", 2 }, c{ "e", "b", "c", 9 };
	rep.add(a);
	rep.add(b);
	rep.add(c);
	aux.add(a);
	aux.add(c);
	auto filter = rep.filter_tip("c"), tip = aux.get_all();
	for (auto i = 0; i < 2; i++) {
		assert(filter.at(i).get_title() == tip.at(i).get_title());
		assert(filter.at(i).get_type() == tip.at(i).get_type());
		assert(filter.at(i).get_description() == tip.at(i).get_description());
		assert(filter.at(i).get_length() == tip.at(i).get_length());
	}
}

void test::test_srv_add_del_mod() {
	repo rep;
	valid val;
	service srv{ rep, val };
	assert(rep.len() == 0);
	activity a{ "a", "b", "c", 2 };
	srv.add("a", "b", "c", 2);
	assert(rep.len() == 1);
	vector<activity> all = srv.get_all(), all_aux;
	all_aux.push_back(a);
	assert(all.at(0).get_title() == all_aux.at(0).get_title());
	assert(all.at(0).get_type() == all_aux.at(0).get_type());
	assert(all.at(0).get_description() == all_aux.at(0).get_description());
	assert(all.at(0).get_length() == all_aux.at(0).get_length());
	srv.modify("a", "b", "c", 12);
	all = srv.get_all();
	assert(all.at(0).get_length() == 12);
	srv.del("a", "c");
	assert(rep.len() == 0);
}

void test::test_srv_find() {
	repo rep;
	valid val;
	service srv{ rep, val };
	srv.add("a", "b", "c", 2);
	const activity a{ "a", "b", "c", 2 };
	const auto found = srv.find("a", "c");
	assert(found.get_title() == a.get_title());
	assert(found.get_type() == a.get_type());
	assert(found.get_description() == a.get_description());
	assert(found.get_length() == a.get_length());
}

void test::test_srv_filter() {
	repo rep;
	valid val;
	service srv{ rep, val };
	srv.add("a", "c", "t", 1);
	srv.add("e", "g", "f", 1);
	srv.add("z", "c", "f", 1);
	activity a{ "a", "c", "f", 1 }, b{ "e", "g", "f", 1 }, c{ "z", "c", "t", 1 };
	vector<activity> filter = srv.filter_description("c"), desc, tip;
	desc.push_back(a);
	desc.push_back(c);
	assert(desc.size() == filter.size());
	assert(desc.at(0).get_description() == filter.at(0).get_description());
	assert(desc.at(1).get_description() == filter.at(1).get_description());
	filter = srv.filter_type("f");
	tip.push_back(b);
	tip.push_back(c);
	assert(tip.size() == filter.size());
	assert(tip.at(0).get_description() == filter.at(0).get_description());
	assert(tip.at(1).get_description() == filter.at(1).get_description());
}

void test::test_srv_sort() {
	repo rep;
	valid val;
	service srv{ rep, val };
	srv.add("e", "c", "t", 1);
	srv.add("a", "g", "f", 2);
	srv.add("z", "a", "f", 3);
	auto sort = srv.sort_by_title();
	assert(sort.at(0).get_title() == "a");
	assert(sort.at(1).get_title() == "e");
	assert(sort.at(2).get_title() == "z");

	sort = srv.sort_by_description();
	assert(sort.at(0).get_description() == "a");
	assert(sort.at(1).get_description() == "c");
	assert(sort.at(2).get_description() == "g");

	sort = srv.sort_by_type_and_length();
	assert(sort.at(0).get_type() == "f");
	assert(sort.at(1).get_type() == "f");
	assert(sort.at(2).get_type() == "t");
}

void test::test_current_list() {
	repo rep;
	valid val;
	service srv{ rep, val };
	srv.add("e", "c", "t", 1);
	srv.add("a", "g", "f", 2);
	srv.add("z", "a", "f", 3);

	srv.add_to_current_list("a");
	auto c = srv.get_current();
	assert(c.size() == 1);
	assert(c.at(0).get_title() == "a");

	try {
		srv.add_to_current_list("b");
		assert(false);
	}
	catch (const RepoException&) {
		assert(true);
	}

	srv.empty_current_list();
	c = srv.get_current();
	assert(c.empty() == true);

	srv.random_activities(2);
	c = srv.get_current();
	assert(c.size() == 2);

	srv.random_activities(2);
	c = srv.get_current();
	assert(c.size() == 3);

	assert(srv.suma_durate() == 6);
}

void test::test_undo() {
	repo rep;
	valid val;
	service srv{ rep, val };
	srv.add("a", "b", "c", 1);
	srv.modify("a", "b", "c", 11);
	srv.del("a", "c");

	vector<activity> l = rep.get_all();
	assert(l.size() == 0);

	srv.undo();
	l = rep.get_all();
	assert(l.at(0).get_title() == "a");
	assert(l.at(0).get_length() == 11);

	srv.undo();
	l = rep.get_all();
	assert(l.at(0).get_title() == "a");
	assert(l.at(0).get_length() == 1);

	srv.undo();
	l = rep.get_all();
	assert(l.size() == 0);

	try {
		srv.undo();
		assert(false);
	}
	catch (const ActivityException& ex) {
		assert(ex.getMsg() == "\nOperatia undo nu se mai poate efectua\n");
	}
}

void test::test_all() {
	test_create();
	test_valid();
	test_repo_add();
	test_repo_delete();
	test_repo_modify();
	test_repo_find();
	test_repo_filter_description();
	test_repo_filter_tip();
	test_srv_add_del_mod();
	test_srv_find();
	test_srv_filter();
	test_srv_sort();
	test_current_list();
	test_undo();
}