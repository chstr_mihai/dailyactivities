#pragma once
#include "service.h"
#include "domain.h"
#include "repo.h"
#include "valid.h"

class test {
private:
	void test_create();

	void test_valid();

	void test_repo_add();

	void test_repo_delete();

	void test_repo_modify();

	void test_repo_find();

	void test_repo_filter_description();

	void test_repo_filter_tip();

	void test_srv_add_del_mod();

	void test_srv_find();

	void test_srv_filter();

	void test_srv_sort();

	void test_current_list();

	void test_undo();
public:
	void test_all();
};