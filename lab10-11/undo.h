#pragma once
#include "domain.h"
#include "repo.h"
#include <qdebug.h>

class ActiuneUndo {
public:
	virtual void doUndo() = 0;

	virtual ~ActiuneUndo() = default;
};


class UndoAdauga : public ActiuneUndo {
private:
	activity act_to_delete;
	repo& rep;

public:
	UndoAdauga(repo& rep, const  activity& p) :rep{ rep }, act_to_delete{ p } {}

	void doUndo() override {
		rep.del(act_to_delete);
	}
};

class UndoSterge : public ActiuneUndo {
private:
	activity act_to_add;
	repo& rep;

public:
	UndoSterge(repo& rep, const activity& p) :rep{ rep }, act_to_add{ p } {}

	void doUndo() override {
		rep.add(act_to_add);
	}
};

class UndoModifica : public ActiuneUndo {
private:
	activity act_to_modify;
	repo& rep;

public:
	UndoModifica(repo& rep, const activity& p) :rep{ rep }, act_to_modify{ p } {}

	void doUndo() override {
		rep.modify(act_to_modify);
	}
};

class UndoFilter : public ActiuneUndo {
private:
	vector<activity> filtered;
	repo& rep;

public:
	UndoFilter(repo& rep, vector<activity> f) : rep{ rep }, filtered{ f } {};

	void doUndo() override {
		for (auto a : filtered)
			rep.add(a);
	}

};