#include "valid.h"

#include <sstream>

void valid::validate(const activity& a) {
	/*
	Functia care valideaza o activitate
	*/
	vector<string> msgs;
	if (a.get_title().empty() == true) msgs.push_back("\nEmpty title!\n");
	if (a.get_description().empty() == true) msgs.push_back("\nEmpty description!\n");
	if (a.get_type().empty() == true) msgs.push_back("\nEmpty type!\n");
	if (a.get_length() <= 0) msgs.push_back("\nLength not valid!\n");
	if (msgs.size() > 0) {
		throw ValidException(msgs);
	}
}

ostream& operator<<(ostream& out, const ValidException& ex) {
	for (const auto& msg : ex.msgs) {
		out << msg << " ";
	}
	return out;
}