#pragma once
#include "domain.h"

#include <string>
#include <vector>

using std::string;
using std::vector;
using std::ostream;

class valid {

public:
	/*
	Functia care valideaza o activitate
	Input: a - o activitate
	Arunca exceptie daca: titlul este vid, descrierea este vida, tipul este vid, durata <= 0
	*/
	void validate(const activity& a);
};

class ValidException {
	vector<string> msgs;
public:
	ValidException(const vector<string>& errors) :msgs{ errors } {}

	vector<string> getMsg()const { return msgs; }


	friend ostream& operator<<(ostream& out, const ValidException& ex);
};